#include <iostream>

using namespace std;

int faktorial(int n) {
    if (n == 0 || n == 1)
        return 1;
    else
        return n * faktorial(n - 1);
}

int kombinasi(int n, int r) {
    return faktorial(n) / (faktorial(r) * faktorial(n - r));
}

void cetakPola(int N) {
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < N - i - 1; ++j)
            cout << " ";
        for (int k = 0; k <= i; ++k) {
            int a = kombinasi(i, k);
            cout << a << " ";
        }
        cout << endl;
    }
}

int main() {
    int N;
    cout << "Masukkan N: ";
    cin >> N;

    cetakPola(N);

    return 0;
}
