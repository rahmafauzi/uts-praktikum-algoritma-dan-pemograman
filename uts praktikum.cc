#include <iostream>
using namespace std;
int main() {
    int a, b;
    cout << "Masukan nilai a: ";
    cin >> a;
    cout << "Masukan nilai b: ";
    cin >> b;

    int HasilPenambahan = a + b;
    int HasilPengurangan = a - b;
    int HasilPerkalian = a * b;
    int HasilPembagian = a / b;

    cout << "Operasi " << a << " + " << b << " = " << HasilPenambahan << endl;
    cout << "Operasi " << a << " - " << b << " = " << HasilPengurangan << endl;
    cout << "Operasi " << a << " * " << b << " = " << HasilPerkalian << endl;
    cout << "Operasi " << a << " / " << b << " = " << HasilPembagian << endl;

    return 0;
}
