#include <iostream>
using namespace std;

int main() {
  int x, y;
  cout << "Masukkan bilangan ke 1       : "; 
  cin >> x; 

  while (x < 0) {
    cout << "Bilangan ke 1 tidak boleh negatif. Masukkan kembali: ";
    cin >> x;
  }

  cout << "Masukkan bilangan ke 2      :  "; 
  cin >> y;

  while (y < 0) {
    cout << "Bilangan ke 2 tidak boleh negatif. Masukkan kembali: ";
    cin >> y;
  }

  if (x < y) {
    cout << "Bilangan Ke 1 < Bilangan Ke 2 :  " << x << " < "<< y << "" << endl;
  } else if (x > y) {
    cout << "Bilangan Ke 1 > Bilangan Ke 2 :  " << x << " > "<< y << "" << endl;
  } else {
     cout << "Bilangan Ke 1 = Bilangan Ke 2 :  " << x << " = "<< y << "" << endl;
  }

  return 0;
}
